import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.unsplash.com',
    headers: {
        Authorization: 'Client-ID kKUuxCY1aC_EmKMWmHL0SQfezxc0JlNmMv2QIQixfA4'
    }
});